cmake_minimum_required(VERSION 3.1)

project(SFMLTest)
include_directories(include)
file(GLOB SOURCES "src/*.cpp")
## If you want to link SFML statically
#set(SFML_STATIC_LIBRARIES TRUE)
IF (WIN32)
	set(FILENAME "APV_9000.exe")
ELSE()
	set(FILENAME "APV_9000")
ENDIF()
## In most cases better set in the CMake cache
#set(SFML_DIR "<sfml root pref>/lib/cmake/SFML")

find_package(SFML 2.5 COMPONENTS graphics audio REQUIRED)
add_executable(${FILENAME} ${SOURCES})
target_link_libraries(${FILENAME} sfml-graphics sfml-audio)
