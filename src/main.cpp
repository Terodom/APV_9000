#include <SFML/Graphics.hpp>

// Constants
const int pxWidth = 800;
const int pxHeight = 600;


int main() {
    sf::RenderWindow window(sf::VideoMode(pxWidth, pxHeight), "Project_Pron");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {

            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        window.draw(shape);
        window.display();
    }

    return 0;
}
